__author__ = 'Lena'
import csv
import auth
import psycopg2
import re
import math
from geopy.geocoders import Nominatim

baza = psycopg2.connect(database=auth.db, host=auth.host, user=auth.user, password=auth.password)

##################################################### BREEDERS #########################################################

# pomozna funkcija, ki sprejme mesto in vrne geo. koordinate (poklicana pri uvozu podatkov o rejcih)
def get_city_coordinates(city):
    geolocator = Nominatim()
    location = geolocator.geocode(city)

    return (location.latitude, location.longitude)



#funkcija, ki uvozi podatke o rejcih
def import_breeders():
    with open('Breeders_data.csv', 'rt', encoding="ISO-8859-1") as csvfile:  #odpremo
        data = csv.reader(csvfile, delimiter=';')  #preberemo
        next(data)  #spustimo header

        c = baza.cursor()  #povezemo kurzor z bazo

        for row in data:  #po vrsticah v podatkih
            last_name, first_name, contact, city, country, id = row[0], row[1], row[2], row[4], row[5], row[
                7]  #shranimo podatke
            coordinates = get_city_coordinates(city + ',' + country)  #dobimo koordinate za mesto
            c.execute(
                "INSERT INTO breeder(breeder_id,first_name, last_name, contact, city, latitude, longitude, country)" \
                "VALUES (%s,%s,%s,%s,%s,%s,%s,%s)",
                [id, first_name, last_name, contact, city, coordinates[0], coordinates[1], country])

        c.close()  #zapremo kurzor
        baza.commit()

##################################################### BREEDS #########################################################

#pomozna funkcija, ki zaokrozi eur
def roundup(x):
    return int(math.ceil(x / 10.0)) * 10

#pomozna funkcija, ki pretvori USD v EUR
def usd_to_eur(usd):
    eur = round(usd * 0.89,0)
    return roundup(eur)

#funkcija za uvoz podatkov o pasmah (in ostalih lastnostih pasme)
def import_breeds():
    with open('Breeds_data_1.csv', 'rt', encoding="ISO-8859-1") as csvfile:
        data = csv.reader(csvfile, delimiter=';')
        next(data)
        c = baza.cursor()

        for row in data:
            row = [None if x == "No info" else x for x in row]  #zamenjamo "No info" z None

            id, breed_name = row[0], row[1]
            hypoallergenic = row[12]

            ################################razdelimo puppy_price na min, max in average################################
            if row[11] is None:
                puppy_price_min, puppy_price_max, puppy_price_average = None, None, None

            else:
                puppy_price = row[11].split(" ")
                if len(puppy_price) == 5:
                    puppy_price_min = usd_to_eur(int(puppy_price[1]))
                    puppy_price_max = usd_to_eur(int(puppy_price[3]))
                    puppy_price_average = usd_to_eur((puppy_price_min + puppy_price_max) / 2)

                else:
                    puppy_price = row[11].split(" ")
                    puppy_price_average = usd_to_eur(int(puppy_price[1]))
                    puppy_price_min, puppy_price_max = None, None

            ##############################razdelimo life_span na min, max in average####################################
            if row[6] is None:
                life_span_min, life_span_max, life_span_average = None, None, None

            else:
                life_span = row[6].split(" ")
                if len(life_span) == 4:
                    life_span_min = float(life_span[0])
                    life_span_max = float(life_span[2])
                    life_span_average = (life_span_min + life_span_max) / 2

                else:
                    life_span = row[6].split(" ")
                    life_span_average = float(life_span[0])
                    life_span_min, life_span_max = None, None

            ############################################################################################################
            poorly_suited_for_hot_weather = row[13]
            well_suited_for_hot_weather = row[14]
            well_suited_for_cold_weather = row[15]
            poorly_suited_for_apartment = row[16]
            well_suited_for_apartment = row[17]
            high_energy, less_exercise, lots_of_exercise = row[18], row[19], row[20]
            wikipedia, photo = row[21], row[22]
            good_With_Kids, cat_Friendly, dog_Friendly, trainability, shedding = row[23], row[24], row[25], row[26], row[27]
            watchdog, intelligence, grooming, popularity, adaptability = row[28], row[29], row[30], row[31], row[32]
            overview, body_type, coat, color_ex, temperament_ex, more_info = row[33], row[34], row[35], row[36], row[37], row[38]

            c.execute("INSERT INTO breed(breed_id,breed_name,life_span_min,life_span_max,life_span_average," \
                      "puppy_price_min,puppy_price_max,puppy_price_average,hypoallergenic,poorly_suited_for_hot_weather,well_suited_for_hot_weather," \
                     "well_suited_for_cold_weather,poorly_suited_for_apartment,well_suited_for_apartment,high_energy," \
                      "less_exercise,lots_of_exercise,wikipedia,photo,good_With_Kids,cat_Friendly,dog_Friendly," \
                      "trainability,shedding,watchdog,intelligence,grooming,popularity,adaptability,overview,body_type," \
                      "coat,color_ex,temperament_ex,more_info) VALUES" \
                      "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                      [int(id), breed_name, life_span_min, life_span_max, life_span_average,
                       puppy_price_min, puppy_price_max, puppy_price_average, hypoallergenic,
                       poorly_suited_for_hot_weather, well_suited_for_hot_weather,
                       well_suited_for_cold_weather, poorly_suited_for_apartment, well_suited_for_apartment,
                       high_energy, less_exercise, lots_of_exercise,
                       wikipedia, photo, good_With_Kids, cat_Friendly, dog_Friendly, trainability, shedding, watchdog,
                       intelligence, grooming, popularity,
                       adaptability, overview, body_type, coat, color_ex, temperament_ex, more_info])
        c.close()
        baza.commit()

################################################### LITTER #############################################################

#pomozna funkcija, ki sprejme ime pasme in vrne id pasme
def get_breed_id(name):
    c = baza.cursor()
    c.execute("SELECT breed_id FROM breed WHERE breed_name = %s", ([name]))  #izberemo ID, kjer breed_name = ime pasme
    id = c.fetchone()  #s tem "ujamemo" ID in ga shranimo
    id = int(id[0])

    c.close()
    return id


def get_breeder_id(name):
    c = baza.cursor()
    c.execute("SELECT breeder_id FROM breeder WHERE first_name = %s", ([name]))  #izberemo ID, kjer breed_name = ime pasme
    id = c.fetchone()  #s tem "ujamemo" ID in ga shranimo
    id = int(id[0])

    c.close()
    return id


#funkcija za uvoz podatkov o leglih
def import_litter():
    with open('Breeders_data.csv', 'rt', encoding="ISO-8859-1") as csvfile:
        data = csv.reader(csvfile, delimiter=';')
        next(data)

        c = baza.cursor()
        for row in data:
            breeder_name = row[1]
            list_of_breeds = row[3].split(", ")  #v vsaki celici je lahko vec pasem zato razbijemo seznam
            list_of_litter_sizes = row[6].split(",")  #isto naredimo pri velikostih legel

            for breed, litter_size in zip(list_of_breeds,list_of_litter_sizes):  #s for zanko gremo po obeh seznamih naenkrat
                c.execute("INSERT INTO litter(breed_id,breeder_id,litter_size) VALUES (%s,%s,%s)", [get_breed_id(breed),get_breeder_id(breeder_name),int(litter_size)])

        c.close()
        baza.commit()

############################################# BREED GROUP & SIZE TYPE #############################################################

#funkcija za uvoz podatkov o skupinah pasem
def import_breed_group():
    with open('Breed_group.csv', 'rt', encoding="ISO-8859-1") as csvfile:
        data = csv.reader(csvfile, delimiter=';')
        next(data)
        c = baza.cursor()

        for row in data:
            breed_group = row[0]
            c.execute("INSERT INTO breed_group(breed_group) VALUES (%s)", [breed_group])

        c.close()
        baza.commit()


#funkcija za uvoz podatkov o velikosti pasem
def import_breed_size():
    with open('Breed_size.csv', 'rt', encoding="ISO-8859-1") as csvfile:
        data = csv.reader(csvfile, delimiter=';')
        next(data)
        c = baza.cursor()

        for row in data:
            size_type = row[0]
            c.execute("INSERT INTO breed_size(size_type) VALUES (%s)", [size_type])

        c.close()
        baza.commit()


#pomozna funkcija, ki sprejme size_type in vrne size_type_id
def get_size_type_id(size):
    c = baza.cursor()
    c.execute("SELECT size_type_id FROM breed_size WHERE size_type = %s", ([size]))
    id = c.fetchone()
    id = int(id[0])
    c.close()
    return id


#uvoz podatkov v "many to many" tabelo; podatki oblike (breed_id,size_type)
def import_breed_size_breed():
    with open('Breed_name+size_type.csv', 'rt', encoding="ISO-8859-1") as csvfile:
        data = csv.reader(csvfile, delimiter=';')
        next(data)
        c = baza.cursor()

        for row in data:
            breed_name = row[0]
            size_type = row[1]
            id = get_breed_id(breed_name)
            size_type_id = get_size_type_id(size_type)
            c.execute("INSERT INTO breed_size_breed(breed_id,size_type_id) VALUES (%s,%s)", [id, size_type_id])

        c.close()
        baza.commit()


#pomozna funkcija, ki sprejme breed_group in vrne breed_group_id
def get_breed_group_id(group):
    c = baza.cursor()
    c.execute("SELECT breed_group_id FROM breed_group WHERE breed_group = %s", ([group]))
    id = c.fetchone()
    id = int(id[0])
    c.close()
    return id


# uvoz podakov v "many to many" tabelo; podatki oblike (breed_id,breed_group)
def import_breed_group_breed():
    c = baza.cursor()

    with open('Breed_name+breed_group.csv', 'rt', encoding="ISO-8859-1") as csvfile:
        data = csv.reader(csvfile, delimiter=';')
        next(data)

        for row in data:
            breed = row[0]
            id = get_breed_id(breed)  #poklicemo funkcijo, da dobimo ID pasme
            list_of_breed_groups = row[1].split(", ")
            for breed_group in list_of_breed_groups:
                breed_group_id = get_breed_group_id(breed_group)
                c.execute("INSERT INTO breed_group_breed(breed_id,breed_group_id) VALUES (%s,%s)", [id, breed_group_id])

    c.close()
    baza.commit()


################################################## COUNTRY #############################################################

#funkcija za poizvedbo o drzavah
def view_country():
    c = baza.cursor()
    c.execute(
        '''CREATE VIEW country AS SELECT DISTINCT country FROM breeder ''')  #pogledamo v tabelo rejcev, kjer so ze shranjene drzave in odstranimo duplikate
    c.close()
    baza.commit()

############################################### OTHER NAME #############################################################

#funkcija za uvoz podatkov o alternativnih imenih
def import_other_breed_name():
    with open('Breeds_data_1.csv', 'rt', encoding="ISO-8859-1") as csvfile:
        data = csv.reader(csvfile, delimiter=';')
        next(data)
        c = baza.cursor()

        for row in data:
            list_of_other_names = row[2].split(", ")
            breed_name = row[1]
            id = get_breed_id(breed_name)
            for other_name in list_of_other_names:
                c.execute("INSERT INTO other_breed_name(breed_id,other_name) VALUES (%s,%s)", [id, other_name])

    c.close()
    baza.commit()


############################################# ORIGIN & TEMPERAMENT & COLOR #############################################

#pomozna funckija, ki vrne origin_id, ce obstaja, v nasprotnem primeru pa None
def get_origin_id_if_exist(origin):
    c = baza.cursor()
    c.execute("SELECT origin_id FROM origin WHERE origin = %s", ([origin]))
    id = c.fetchone()
    if id is not None:
        id = int(id[0])

    c.close()
    return id


#funkcija za uvoz podatkov o izvoru
def import_origin():
    with open('Breeds_data_1.csv', 'rt', encoding="ISO-8859-1") as csvfile:
        data = csv.reader(csvfile, delimiter=';')
        next(data)
        c = baza.cursor()

        for row in data:
            list_of_origins = row[3].split(", ")
            for origin in list_of_origins:
                id = get_origin_id_if_exist(origin)  #id je None ali integer
                if id is None and "No info" not in origin:  #ce je id = None, pomeni, da ta origin se ni v bazi
                    c.execute("INSERT INTO origin(origin) VALUES (%s)", [origin])

    c.close()
    baza.commit()


#pomozna funckija, ki vrne temperament_id, ce obstaja, v nasprotnem primeru pa None
def get_temperament_id_if_exist(temperament):
    c = baza.cursor()
    c.execute("SELECT temperament_id FROM temperament WHERE temperament = %s", ([temperament]))
    id = c.fetchone()
    if id is not None:
        id = int(id[0])

    c.close()
    return id


#funkcija za uvoz podatkov o temperamentu
def import_temperament():
    with open('Breeds_data_1.csv', 'rt', encoding="ISO-8859-1") as csvfile:
        data = csv.reader(csvfile, delimiter=';')
        next(data)
        c = baza.cursor()

        for row in data:
            list_of_temperaments = row[7].split(", ")
            for temperament in list_of_temperaments:
                id = get_temperament_id_if_exist(temperament)
                if id is None:
                    c.execute("INSERT INTO temperament(temperament) VALUES (%s)", [temperament])

    c.close()
    baza.commit()


#pomozna funckija, ki vrne color_id, ce obstaja, v nasprotnem primeru pa None
def get_color_id_if_exist(color):
    c = baza.cursor()
    c.execute("SELECT color_id FROM color WHERE color = %s", ([color]))
    id = c.fetchone()
    if id is not None:
        id = int(id[0])

    c.close()
    return id


#funkcija za uvoz podatkov o barvah
def import_color():
    with open('Breeds_data_1.csv', 'rt', encoding="ISO-8859-1") as csvfile:
        data = csv.reader(csvfile, delimiter=';')
        next(data)
        c = baza.cursor()

        for row in data:
            list_of_colors = row[10].split(", ")
            for color in list_of_colors:
                id = get_color_id_if_exist(color)
                if id is None:
                    c.execute("INSERT INTO color(color) VALUES (%s)", [color])

    c.close()
    baza.commit()


#funkcija za uvoz podakov v "many to many" tabele (breed_origin,breed_color,breed_temperament)
# podatki oblike npr. (breed_id,origin_id)

def import_many_to_many():
    c = baza.cursor()

    with open('Breeds_data_1.csv', 'rt', encoding="ISO-8859-1") as csvfile:
        data = csv.reader(csvfile, delimiter=';')
        next(data)

        for row in data:
            breed = row[1]
            breed_id = get_breed_id(breed)

            list_of_origins = row[3].split(", ")
            for origin in list_of_origins:
                if "No info" not in origin:
                    origin_id = get_origin_id_if_exist(origin)
                    c.execute("INSERT INTO origin_breed(breed_id,origin_id) VALUES (%s,%s)", [breed_id, origin_id])

            list_of_colors = row[10].split(", ")
            for color in list_of_colors:
                if "No info" not in color:
                    color_id = get_color_id_if_exist(color)
                    c.execute("INSERT INTO color_breed(breed_id,color_id) VALUES (%s,%s)", [breed_id, color_id])

            list_of_temperaments = row[7].split(", ")
            for temperament in list_of_temperaments:
                if "No info" not in temperament:
                    temperament_id = get_temperament_id_if_exist(temperament)
                    c.execute("INSERT INTO temperament_breed(breed_id,temperament_id) VALUES (%s,%s)",
                              [breed_id, temperament_id])

    c.close()
    baza.commit()


#################################################### WEIGHT & HEIGHT ###################################################

#funkcija, ki pretvori inche v cm
def inches_to_cm(inches):
    cm = 2.54 * inches
    return cm

#funkcija, ki pretvori pounds v kg
def pounds_to_kg(pounds):
    kg = 0.453592 * pounds
    return kg

#funkcija, ki uredi podatke o tezah in visinah
def parse_height_weight(data_to_parse,unit):
    # pripravimo slovar za vnasanje visin/tez
    data_dict = {}
    if data_to_parse != None:
        # razdelimo niz po vejicah, da ločimo primere
        for v in data_to_parse.split(","):
            m = re.match(
                r'([MF]?)[a-z]*[ :]*([0-9.]+)[- ]*([0-9.]*) ([a-z]+)(?: \(([0-9.]+)[- ]*([0-9.]*) ([a-z]+)\))?', v)
            # *: 0 ali več ponovitev, +: 1 ali več ponovitev, ?: 0 ali 1 ponovitev
            # skupine so:
            # 1. prva črka spola
            # 2. prva spodnja meja
            # 3. prva zgornja meja
            # 4. prva enota
            # 5. druga spodnja meja
            # 6. druga zgornja meja
            # 7. druga enota
            if m != None:
                i = 2 #indeks za minimum
                conv = False
                # pogledamo, ali so cm/kg na drugem mestu
                if m.group(7) != None and m.group(7)[:2] == unit:
                    i = 5 #indeks za minimum

                # ali pa jih sploh nimamo
                elif m.group(4)[:2] != unit:
                    conv = True

                #preverimo ali imamo dve vrednosti ali samo eno
                if m.group(i + 1) == '': #ce je vrednost ena sama, je to kar povprecje
                    average = float(m.group(i))
                    min,max = None,None
                else: #ce sta dve vrednosti imamo min in max
                    min,max = float(m.group(i)),float(m.group(i + 1))
                    average = round((min + max)/2,1)

                # ali moramo pretvarjati?
                if conv:
                    #ce imamo dve vrednosti
                    if min != None and max != None:
                        #preverimo enote, da vemo, katero funkcijo za pretvarjanje uporabiti
                        if unit == "cm":
                            min = round(inches_to_cm(min),1)
                            max = round(inches_to_cm(max),1)
                        else:
                            min = round(pounds_to_kg(min),1)
                            max = round(pounds_to_kg(max),1)
                    #podobno kot zgoraj
                    if unit == "cm":
                        average = round(inches_to_cm(average),1)
                    else:
                        average = round(pounds_to_kg(average),1)

                data = [min, max, average]
                # če spol ni znan, zapišemo za oba
                if m.group(1) == '':
                    data_dict['F'] = data
                    data_dict['M'] = data
                # sicer pa samo za podani spol
                else:
                    data_dict[m.group(1)] = data
    return data_dict

#pomozna funkcija, ki pregleda manjkajoce podatke in jih nastavi na None
def add_None(dict):
    if len(dict) == 1: #ce podatek samo za en spol
        if "M" in dict.keys():
            dict["F"] = [None,None,None]
        elif "F" in dict.keys():
            dict["M"] = [None,None,None]
    elif dict == {}: #ce ni podatkov za noben spol
        dict["M"],dict["F"] = [None,None,None],[None,None,None]
    return dict

#funkcija za uvoz podatkov o visinah in tezah
def import_height_weight():
    with open('Breeds_data_1.csv', 'rt', encoding="ISO-8859-1") as csvfile:
        data = csv.reader(csvfile, delimiter=';')
        next(data)
        c = baza.cursor()
        for row in data:
            row = [None if x == "No info" else x for x in row]  #zamenjamo "No info" z None
            h = row[8]
            w = row[9]

            #dobimo najprej ostale podatke
            breed_id = get_breed_id(row[1])
            c.execute("SELECT size_type_id FROM breed_size_breed WHERE breed_id = %s", ([breed_id]))
            size_type_id = c.fetchone()

            c.execute("SELECT size_type FROM breed_size WHERE size_type_id = %s", ([size_type_id[0]]))
            size_type = c.fetchone()

            #uredimo podatke o visinah in tezah s funkcijo parse_height_weight
            height = parse_height_weight(h,"cm")
            weight = parse_height_weight(w,"kg")

            #uredimo manjkajoce podatke
            height,weight = add_None(height),add_None(weight)

            #zdruzimo tezo in visino v en slovar, da bomo lahko vnesli v bazo
            height_weight = {}
            height_value,weight_value = height["M"],weight["M"]
            height_weight["M"] = height_value + weight_value
            height_value,weight_value = height["F"],weight["F"]
            height_weight["F"] = height_value + weight_value

            for key,value in height_weight.items():
                sex = key
                min_height,max_height,average_height = value[0],value[1],value[2]
                min_weight,max_weight,average_weight = value[3],value[4],value[5]
                c.execute("INSERT INTO height_weight (breed_id,sex,size_type,min_height,max_height,average_height," \
                "min_weight,max_weight,average_weight) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                     [breed_id,sex,size_type[0],min_height,max_height,average_height,min_weight,max_weight,average_weight])

    c.close()
    baza.commit()

############################### dodatni poizvedbi ##########################################
#poizvedbi view1 in view2 se ujemata z poizvedbama info1 in info2 (narejeni online)

#Tabela številskih lastnosti po pasmah
def view1():
    c = baza.cursor()
    c.execute(
        '''CREATE VIEW view1 AS SELECT breed_name, good_with_kids, cat_friendly,
            dog_friendly, trainability ,shedding ,watchdog ,intelligence ,grooming,
            popularity ,adaptability,
            CASE WHEN hypoallergenic='TRUE' THEN 'Yes'
            ELSE 'No'
            END AS hypo
            FROM breed AS BR ''')
    c.close()
    baza.commit()

#Krajši opis pasem
def view2():
    c = baza.cursor()
    c.execute(
        '''CREATE VIEW view2 AS SELECT breed_name
                ,array_to_string( array( SELECT other_name
                                              FROM other_breed_name AS IME
                                              WHERE BR.breed_id = IME.breed_id ), ', ' ) AS other_names

                ,array_to_string( array( SELECT origin
                                              FROM origin_breed AS OB
                                              JOIN origin AS ORG ON OB.origin_id=ORG.origin_id
                                              WHERE OB.breed_id = BR.breed_id ), ', ' ) AS origin

                ,array_to_string( array( SELECT breed_group
                                              FROM breed_group_breed AS BGB
                                              JOIN breed_group AS BG ON BGB.breed_group_id=BG.breed_group_id
                                              WHERE BGB.breed_id = BR.breed_id ), ', ' ) AS breed_group

                ,array_to_string( array( SELECT size_type
                                              FROM breed_size_breed AS BSB
                                              JOIN breed_size AS BS ON BSB.size_type_id=BS.size_type_id
                                              WHERE BSB.breed_id = BR.breed_id ), ', ' ) AS breed_size

                ,CASE WHEN life_span_average IS NULL AND life_span_min IS NULL THEN 'No information'
                      WHEN life_span_average IS NOT NULL AND life_span_min IS NULL THEN to_char(life_span_average,'99') || ' years'
                      ELSE 'From' || to_char(life_span_min,'99') || ' to' || to_char(life_span_max,'99') || ' years'
                      END AS life_span

                ,CASE WHEN puppy_price_average IS NULL AND puppy_price_min IS NULL THEN 'No information'
                      WHEN puppy_price_average IS NOT NULL AND puppy_price_min IS NULL THEN to_char(puppy_price_average,'9999') || ' EUR'
                      ELSE 'From' || to_char(puppy_price_min,'9999') || ' to' || to_char(puppy_price_max,'9999') || ' EUR'
                      END AS puppy_price

                ,array_to_string( array( SELECT color
                                              FROM color_breed AS CB
                                              JOIN color AS CO ON CB.color_id=co.color_id
                                              WHERE CB.breed_id = BR.breed_id ), ', ' ) AS color

                ,array_to_string( array( SELECT temperament
                                              FROM temperament_breed AS TB
                                              JOIN temperament AS TEM ON TB.temperament_id=TEM.temperament_id
                                              WHERE TB.breed_id = BR.breed_id ), ', ' ) AS temperament

                FROM breed AS BR ''')

    c.close()
    baza.commit()