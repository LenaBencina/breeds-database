# ALL ABOUT DOG BREEDS #

This is a school project - creating database and use it to build an app.

Our database looks like this ![ER.png](https://bitbucket.org/repo/jBE7Ez/images/3058843577-ER.png)

We used python for creating (Breeds_db.py) database and importing (import_csv_db.py) data from cvs files. (We parse data with parseHub.) All data are located in a map called Data.

All files needed for the *Shiny app* are in a map called Shiny (A new way to build web applications with R).

### SIMPLE SHINY APP FOR DOG BREEDS REVIEW ###

* In the first tab you can choose a breed that you're interested in and read all about it. 
![breeds1.jpg](https://bitbucket.org/repo/jBE7Ez/images/2509336161-breeds1.jpg)

* In the second tab you can choose country or/and breed and look up for breeders nearby (NOTE: breeders data are not real!).
![breeds2.jpg](https://bitbucket.org/repo/jBE7Ez/images/1805885063-breeds2.jpg)

* In the third tab you can find your own match by choosing attributes that matches your wishes.
![breeds3.jpg](https://bitbucket.org/repo/jBE7Ez/images/3582420198-breeds3.jpg)

* Fourth tab is a little analysis about some of the breed attributes - diagrams, barplot, tables,...
![breeds4.jpg](https://bitbucket.org/repo/jBE7Ez/images/2748977898-breeds4.jpg)